﻿using System.Collections.Generic;
using System;
using System.Linq;

public static class ExtensionMethods
{
    /// <summary>
    /// Extracts query parameters
    /// </summary>
    /// <returns>Dictionary with query parameters</returns>
    public static Dictionary<string, string> GetQueryParameters(this Uri uri)
    {
        //remove question mark from the beginning
        var query = uri.Query.Replace("?", "");

        return query.Split('&').Select(q => q.Split('='))
               .ToDictionary(k => k[0], v => v[1]);
    }
}
