Niniejsze skrypty sa czescia duzo wiekszego projektu i realizuja obsluge WebView w aplikacji zarowno na platformach desktopowych, 
jak i na urzadzenia mobilne. Podstawa jest tutaj interfejs IWebView, ktory posiada wszystkie potrzebne do uzycia funkcje oraz pola.
W projekcie uzywane sa dwa pluginy odpowiedzialne za natywna obsluge WebView - na platformy desktopowe oraz mobilne.
W zwiazku z tym, ze sa to produkty zewnetrzne, ich implementacji w niniejszej paczce nie zamiescilem, a jedynie
sztuczne klasy z odpowiednimi polami. Zaimplementowano dwie klasy - DesktopWebview oraz MobileWebview. Obie obsluguja klasy z pluginow i 
implementuja interfejs IWebView. Klasa DesktopWebview w pelni wystarcza do aktualnych celow, jednak MobileWebview stanowi jedynie
podstawe do kolejnych klas pochodnych. Jako, ze w aplikacji webview uzywane jest aby otrzymac pewne informacje, musza one 
obslugiwac niestandardowe schematy uri. W AzureMobileWebview uzytkownik jest przekierowywany w ostatniej fazie 
na adres urn:ietf:wg:oauth:2.0:oob ... Implementacja pluginowa WebView nie potrafi obsluzyc tego schematu,
dlatego jest to robione reczne w tej klasie. W CheckoutMobileWebview jest po prostu dodawany odpowiedni schemat i plugin rozpoznaje
go w pelni. Za tworzenie odpowiednich rodzajow WebView odpowiada WebviewFactory, gdzie na podstawie przekazanego stringa zwracany
jest odpowiedni typ. 