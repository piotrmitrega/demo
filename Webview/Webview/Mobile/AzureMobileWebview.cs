﻿using System;

namespace PiotrMitrega.Test
{
    public class AzureMobileWebview : MobileWebview
    {
        private const string ReceivedResponseUrl = "";

        protected override void OnWebviewFailedToLoad(WebView _webview, string _error)
        {
            if (!IsWebviewEventTargetValid(_webview)) return;

            //if this is authorization code received - try parse unhandled url scheme
            if (_error.Contains(ReceivedResponseUrl))
            {
                try
                {
                    var message = new AzureWebViewMessage(_error);
                    OnResponseReceived(message);
                }
                catch (Exception ex)
                {
                    InternalOnError(ex.ToString());
                }
            }
            
            //if not, this is unknown response
            else
            {
                InternalOnError("Unknown response : \n" + _error);
            }
        }
    }
}