﻿using UnityEngine;
using System;

namespace PiotrMitrega.Test
{
    public abstract class MobileWebview : MonoBehaviour, IWebview
    {
        public event Action<WebViewMessage> ResponseReceived;
        public event Action<string> Error;
        public event Action Cancelled;

        protected bool isOpened;
        protected WebView webview;

        /// <summary>
        /// Initialization - Setup events
        /// </summary>
        public virtual void Init()
        {
            WebView.DidShowEvent += OnWebviewShow;
            WebView.DidHideEvent += OnWebviewHide;
            WebView.DidFailLoadWithErrorEvent += OnWebviewFailedToLoad;
            WebView.DidFinishLoadEvent += OnWebviewFinishLoad;
            WebView.DidDestroyEvent += OnWebviewDestroy;
            WebView.DidReceiveMessageEvent += OnWebviewReceivedMessage;
        }

        /// <summary>
        /// Creates webview
        /// </summary>
        protected virtual void CreateWebview()
        {
            try
            {
                if (webview != null) return;
                webview = gameObject.AddComponent<WebView>();   
                //more actions...
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        /// <summary>
        /// Creates the webview if needed and loads the url
        /// </summary>
        /// <param name="url"></param>
        public virtual void Load(string url)
        {
            try
            {
                CreateWebview();

                if (webview == null) return;
                webview.LoadRequest(url);
                webview.Show();
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }

       /// <summary>
       /// Closes the webview
       /// </summary>
        public virtual void Close()
        {
            if (webview != null) Destroy(webview);
        }

        /// <summary>
        /// Simply check if given webview is this instance's webview.
        /// </summary>
        /// <param name="_webview">The _webview.</param>
        /// <returns></returns>
        protected bool IsWebviewEventTargetValid(WebView _webview)
        {
            return _webview == webview;
        }

        /// <summary>
        /// Called when webview failed to load given url.
        /// </summary>
        /// <param name="_webview">The _webview.</param>
        /// <param name="_error">The _error.</param>
        protected virtual void OnWebviewFailedToLoad(WebView _webview, string _error)
        {
            if (!IsWebviewEventTargetValid(_webview)) return;
            InternalOnError(_error);
        }

        /// <summary>
        /// Called when webview hides.
        /// </summary>
        /// <param name="_webview">The _webview.</param>
        protected virtual void OnWebviewHide(WebView _webview)
        {
            if (!IsWebviewEventTargetValid(_webview)) return;
            OnCancelled();
        }

        /// <summary>
        /// Called when webview is being destroyed.
        /// </summary>
        /// <param name="_webview">The _webview.</param>
        protected virtual void OnWebviewDestroy(WebView _webview)
        {
            if (!IsWebviewEventTargetValid(_webview)) return;

            //destroyed webview means cancelled interaction
            OnCancelled();
        }

        /// <summary>
        /// Called when webview receives message from read url scheme.
        /// </summary>
        /// <param name="_webview">The _webview.</param>
        /// <param name="_message">The _message.</param>
        protected virtual void OnWebviewReceivedMessage(WebView _webview, WebViewMessage _message) { }

        /// <summary>
        /// Called when webview shows.
        /// </summary>
        /// <param name="_webview">The _webview.</param>
        protected virtual void OnWebviewShow(WebView _webview) { }

        /// <summary>
        /// Called when webview finish loading.
        /// </summary>
        /// <param name="_webview">The _webview.</param>
        protected virtual void OnWebviewFinishLoad(WebView _webview) { }

        /// <summary>
        /// Called when user cancel the interaction
        /// </summary>
        protected void OnCancelled()
        {
            if (isOpened && Cancelled != null) Cancelled();
            isOpened = false;
        }

        /// <summary>
        /// Called when response is successfully received
        /// </summary>
        /// <param name="message">The message.</param>
        protected virtual void OnResponseReceived(WebViewMessage message)
        {
            Close();
            if (ResponseReceived != null) ResponseReceived(message);
        }

        /// <summary>
        /// Called on error
        /// </summary>
        /// <param name="error"></param>
        protected void InternalOnError(string error)
        {
            isOpened = false;
            Close();

            if (Error != null)
            {
                Error(error);
            }
        }

        /// <summary>
        /// Called when MonoBehaviour is destroyed
        /// </summary>
        protected virtual void OnDestroy()
        {
            WebView.DidShowEvent -= OnWebviewShow;
            WebView.DidHideEvent -= OnWebviewHide;
            WebView.DidFailLoadWithErrorEvent -= OnWebviewFailedToLoad;
            WebView.DidFinishLoadEvent -= OnWebviewFinishLoad;
            WebView.DidDestroyEvent -= OnWebviewDestroy;
            WebView.DidReceiveMessageEvent -= OnWebviewReceivedMessage;
        }
    }
}