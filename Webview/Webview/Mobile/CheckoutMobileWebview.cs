﻿namespace PiotrMitrega.Test
{
    public class CheckoutMobileWebview : MobileWebview
    {
        protected override void CreateWebview()
        {
            base.CreateWebview();

            //add url scheme to be recognized
            if(webview != null) webview.AddNewURLSchemeName(WebviewConfiguration.ThemoonProtocol);
        }

        protected override void OnWebviewReceivedMessage(WebView _webview, WebViewMessage _message)
        {
            OnResponseReceived(_message);
        }
    }
}