﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace PiotrMitrega.Test
{
    public class AzureWebViewMessage : WebViewMessage
    {
        public AzureWebViewMessage(string response)
        {
            var startIndex = response.LastIndexOf("code=", StringComparison.Ordinal) + 5;
            var commaIndex = response.IndexOf(",", startIndex, StringComparison.Ordinal);
            var braceIndex = response.IndexOf("}", startIndex, StringComparison.Ordinal);
            var endIndex = Mathf.Min(commaIndex > 0 ? commaIndex : int.MaxValue, braceIndex > 0 ? braceIndex : int.MaxValue);
            var code = response.Substring(startIndex, endIndex - startIndex);

            Arguments = new Dictionary<string, string>();
            Arguments.Add(WebviewConfiguration.UrlParameters.Code, code);
        }
    }
}