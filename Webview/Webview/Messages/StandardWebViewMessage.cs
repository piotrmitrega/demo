﻿using System;

namespace PiotrMitrega.Test
{
    public class StandardWebViewMessage : WebViewMessage
    {
        public StandardWebViewMessage(string url)
        {
            var uri = new Uri(url);

            SchemeName = uri.Scheme;
            Arguments = uri.GetQueryParameters();
            Host = uri.Host;
        }
    }
}