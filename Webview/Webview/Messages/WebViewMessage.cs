﻿using System.Collections.Generic;

namespace PiotrMitrega.Test
{
	/// <summary>
	/// WebViewMessage is a wrapped data which is sent from <see cref="WebView"/> to Unity.
	/// </summary>
	public class WebViewMessage  
	{
		/// <summary>
		/// Gets the scheme name.
		/// </summary>
		/// <value>The scheme name.</value>
		public string SchemeName 						
		{ 
			get; 
			protected set; 
		}

		/// <summary>
		/// Gets the host name.
		/// </summary>
		/// <value>The host name.</value>
		public string Host	 							
		{ 
			get; 
			protected set; 
		}

		/// <summary>
		/// Gets the arguments. Each key-value pair represents name and value of the argument
		/// </summary>
		/// <value>The arguments.</value>
		public Dictionary<string, string> Arguments  	
		{ 
			get; 
			protected set; 
		}

		protected WebViewMessage ()
		{
			SchemeName	= null;
			Host		= null;
			Arguments	= null;
		}
	}
}