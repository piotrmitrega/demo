﻿using System;

namespace PiotrMitrega.Test
{
    /// <summary>
    /// Represents webview handlers
    /// </summary>
    public interface IWebview
    {
        /// <summary>
        /// Occurs when valid response is received.
        /// </summary>
        event Action<WebViewMessage> ResponseReceived;

        /// <summary>
        /// Occurs on webview error.
        /// </summary>
        event Action<string> Error;

        /// <summary>
        /// Occurs when user cancel the interaction.
        /// </summary>
        event Action Cancelled;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void Init();

        /// <summary>
        /// Loads the specified URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        void Load(string url);

        /// <summary>
        /// Closes this instance.
        /// </summary>
        void Close();
    }
}