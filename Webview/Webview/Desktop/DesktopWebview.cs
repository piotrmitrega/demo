﻿using UnityEngine;
using System;

namespace PiotrMitrega.Test
{
    [RequireComponent(typeof(RectTransform))]
    public class DesktopWebview : Browser, IWebview
    {
        public event Action<WebViewMessage> ResponseReceived;
        public event Action<string> Error;
        public event Action Cancelled;

        protected bool isOpened;

        public override void Init()
        {
            base.Init();
 
            var uiHandler = gameObject.AddComponent<SimpleBrowserUI>();
            uiHandler.Init(this);

            onFetchError += OnFetchError;
        }

        /// <summary>
        /// Called when webview fail to load current url
        /// </summary>
        /// <param name="error">The error.</param>
        protected void OnFetchError(JSONNode error)
        {
            try
            {
                //workaround for not recognizing custom url schemes
                var message = new StandardWebViewMessage(error["url"]);
                OnResponseReceived(message);
            }
            catch (Exception ex)
            {
                InternalOnError(ex.ToString());
            }
        }


        /// <summary>
        /// Called when user cancel the interaction
        /// </summary>
        protected void OnCancelled()
        {
            if (isOpened && Cancelled != null) Cancelled();
            isOpened = false;
        }

        /// <summary>
        /// Called on error
        /// </summary>
        /// <param name="error"></param>
        protected void InternalOnError(string error)
        {
            isOpened = false;
            Close();

            if (Error != null)
            {
                Error(error);
            }
        }

        /// <summary>
        /// Called when MonoBehaviour is destroyed.
        /// </summary>
        protected override void OnDestroy()
        {
            base.OnDestroy();
            OnCancelled();
        }

        protected virtual void OnResponseReceived(WebViewMessage message)
        {
            try
            {
                if (ResponseReceived != null) ResponseReceived(message);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            finally
            {
                Close();
            }
        }

        public void Load(string url)
        {
            //Url property loads url automatically
            Url = url;
        }

        public void Close()
        {
            try
            {
                isOpened = false;
                Destroy(gameObject);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }
    }
}