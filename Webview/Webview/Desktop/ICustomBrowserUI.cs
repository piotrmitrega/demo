﻿namespace PiotrMitrega.Test
{
    /// <summary>
    /// Represent IBrowserUI class with Init method
    /// </summary>
    public interface ICustomBrowserUI : IBrowserUI
    {
        /// <summary>
        /// Initializes the instance with given browser
        /// </summary>
        /// <param name="parentBrowser">The parent browser.</param>
        void Init(Browser parentBrowser);
    }
}