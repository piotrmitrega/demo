﻿using System;
using UnityEngine;
using System.Collections;

namespace PiotrMitrega.Test
{
    public class WebviewConfiguration : MonoBehaviour
    {
        public class UrlParameters
        {
            public const string Code = "code";
            public const string OrderRef = "orderRef";
        }

        public const string ThemoonProtocol = "themoon";

        public const string CheckoutWebviewName = "CheckoutWebview";
        public const string AzureWebviewName = "AzureWebview";
        public const string DesktopWebviewName = "StandaloneWebview";
    }
}