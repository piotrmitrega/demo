﻿using UnityEngine;
using System;

namespace PiotrMitrega.Test
{
    /// <summary>
    /// Native plugin class
    /// </summary>
    public class WebView : MonoBehaviour
    {
        public static event Action<WebView> DidShowEvent;
        public static event Action<WebView> DidHideEvent;
        public static event Action<WebView, string> DidFailLoadWithErrorEvent;
        public static event Action<WebView> DidFinishLoadEvent;
        public static event Action<WebView> DidDestroyEvent;
        public static event Action<WebView, WebViewMessage> DidReceiveMessageEvent;

        public void LoadRequest(string url)
        {

        }

        public void Show()
        {

        }

        public void AddNewURLSchemeName(string scheme)
        {

        }
    }
}