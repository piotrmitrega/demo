﻿using UnityEngine;
using System.Collections;
using System;

namespace PiotrMitrega.Test
{
    public class Browser : MonoBehaviour
    {
        protected Action<JSONNode> onFetchError;

        public string Url { get; set; }

        public virtual void Init() { }
        protected virtual void OnDestroy() { }
    }

    public class JSONNode
    {
        public string this[string key]
        {
            get
            {
                return "Not available";
            }
        }
    }
}