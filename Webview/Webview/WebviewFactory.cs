﻿using System;
using UnityEngine;
using System.Collections;

namespace PiotrMitrega.Test
{
    public class WebviewFactory
    {
        /// <summary>
        /// Factory method that creates a proper webview.
        /// </summary>
        /// <param name="webviewName">The type.</param>
        /// <returns></returns>
        public static IWebview CreateWebview(string webviewName)
        {
            var type = ResolveType(webviewName);
            var webviewGameObject = new GameObject(webviewName);
            var webview = (IWebview)webviewGameObject.AddComponent(type);
            webview.Init();

            return webview;
        }

        /// <summary>
        /// Resolves the type from webview name.
        /// </summary>
        /// <param name="webviewName">Name of the webview.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Unknown type;webviewName</exception>
        private static Type ResolveType(string webviewName)
        {
            switch (webviewName)
            {
                case WebviewConfiguration.CheckoutWebviewName:
                    return GetPlatformDependentType(typeof(CheckoutMobileWebview));

                case WebviewConfiguration.AzureWebviewName:
                    return GetPlatformDependentType(typeof(AzureMobileWebview));

                case WebviewConfiguration.DesktopWebviewName:
                    return GetPlatformDependentType(typeof(DesktopWebview));

                default:
                    throw new ArgumentException("Unknown type", "webviewName");
            }
        }

        /// <summary>
        /// Assuming given type is mobile type, this functions checks if we are running standalone and modify the type if needed
        /// </summary>
        /// <param name="mobileType">The type.</param>
        /// <returns></returns>
        private static Type GetPlatformDependentType(Type mobileType)
        {
            var isStandaloneOrEditor = (Application.isEditor
                || (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor));

            return isStandaloneOrEditor ? typeof(DesktopWebview) : mobileType;
        }
    }
}