﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct SubimageParameters
{
    public Vector2 position;
    public List<int> contour;

    public SubimageParameters(Vector2 position, List<int> contour)
    {
        this.position = position;
        this.contour = contour;
    }
}
