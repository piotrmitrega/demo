﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PiotrMitrega.Ocr
{
    public class Image : Array2D
    {
        public const int BackgroundPixel = 1;
        public const int ForegroundPixel = 0;

        public const int AdaptiveThresholdSurroundSize = 3;

        /// <summary>
        /// Returns new image in grayscale
        /// </summary>
        /// <param name="colors"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Image Grayscale(Color[] colors, int width, int height, GrayscaleType type = GrayscaleType.Lumosity)
        {
            var value = 0.0f;

            var values = new int[colors.Length];
            for (var i = 0; i < colors.Length; i++)
            {
                switch (type)
                {
                    case GrayscaleType.Average:
                        value = (colors[i].r + colors[i].g + colors[i].b) / 3f;
                        break;

                    case GrayscaleType.Lumosity:
                        value = (0.21f * colors[i].r + 0.72f * colors[i].g + 0.07f * colors[i].b);
                        break;

                    case GrayscaleType.Lightness:
                        value = (Mathf.Max(colors[i].r, colors[i].g, colors[i].b) + Mathf.Min(colors[i].r, colors[i].g, colors[i].b)) / 2f;
                        break;
                }

                values[i] = (int)(value * 255f);
            }

            return new Image(width, height, values);
        }

        public Image(int width, int height, int[] data) : base(width, height, data)
        {

        }

        /// <summary>
        /// Rotates this image and return a new one
        /// </summary>
        /// <param name="degrees"></param>
        /// <returns></returns>
        public Image Rotate(int degrees)
        {
            var newData = new List<int>();
            var newWidth = 0;
            var newHeight = 0;
           
            if (degrees == -90)
            {
                for (var x = width - 1; x >= 0; x--)
                {
                    for (var y = 0; y < height; y++)
                    {
                        newData.Add(this[x, y]);
                    }
                }

                newWidth = height;
                newHeight = width;
            }
            else if (degrees == 90)
            {
                for (var x = 0; x < width; x++)
                {
                    for (var y = height - 1; y >= 0 ; y--)
                    {
                        newData.Add(this[x, y]);
                    }
                }

                newWidth = height;
                newHeight = width;
            }
            else
            {
                //don't need to use any other angle
                return this;
            }

            return new Image(newWidth, newHeight, newData.ToArray());
        }

        /// <summary>
        /// Creates copy of this image
        /// </summary>
        /// <returns></returns>
        public Image Clone()
        {
            return new Image(width, height, Data);
        }

        /// <summary>
        /// Gets a Texture2D from this image
        /// </summary>
        /// <returns></returns>
        public Texture2D CreateTexture()
        {
            var texture = new Texture2D(width, height);
            Color[] colors = null;

            //image not thresholded
            if (Data.Max() > 1)
            {
                colors = (from pixel in Data
                          select new Color(pixel / 255f, pixel / 255f, pixel / 255f, 1)).ToArray();
            }
            else
            {
                colors = (from pixel in Data
                          select new Color(pixel, pixel, pixel, 1)).ToArray();
            }

            texture.SetPixels(colors);
            texture.Apply();

            return texture;
        }
       
        /// <summary>
        /// Gets bounding box for given pixels array
        /// </summary>
        /// <param name="pixels"></param>
        /// <param name="subimageParameters"></param>
        /// <returns></returns>
        public Image GetBoundingBox(List<int> pixels, out SubimageParameters subimageParameters)
        {
            var coordinates =
                 (from pixel in pixels
                  select IndexToPosition(pixel)).ToList();

            var minPoint = new Vector2(coordinates.Min(v => v.x), coordinates.Min(v => v.y));
            var maxPoint = new Vector2(coordinates.Max(v => v.x), coordinates.Max(v => v.y));
            var center = new Vector2(minPoint.x + ((maxPoint.x - minPoint.x) / 2f), minPoint.y + ((maxPoint.y - minPoint.y) / 2f));

            var contour = new List<int>();

            for (var x = 0; x <= maxPoint.x - minPoint.x; x++)
            {
                contour.Add(PositionToIndex(new Vector2(minPoint.x + x, minPoint.y)));
                contour.Add(PositionToIndex(new Vector2(minPoint.x + x, maxPoint.y)));
            }

            for (var y = 0; y <= maxPoint.y - minPoint.y; y++)
            {
                contour.Add(PositionToIndex(new Vector2(minPoint.x, minPoint.y + y)));
                contour.Add(PositionToIndex(new Vector2(maxPoint.x, minPoint.y + y)));
            }

            subimageParameters = new SubimageParameters(center, contour);

            var box = new List<int>();
            for (var y = 0; y <= maxPoint.y - minPoint.y; y++)
            {
                for (var x = 0; x <= maxPoint.x - minPoint.x; x++)
                {
                    box.Add(GetElement((int)minPoint.x + x, (int)minPoint.y + y));
                }
            }

            return new Image((int)(maxPoint.x - minPoint.x + 1), (int)(maxPoint.y - minPoint.y + 1), box.ToArray());
        }
        
        /// <summary>
        /// Process adaptive threshold on this image and returns a new one
        /// </summary>
        /// <returns></returns>
        public Image AdaptiveThreshold()
        {
            try
            {

                var thresholded = new int[Data.Length];
                var globalMean = (int)(Data.Sum() / (float)Data.Length);
                var index = 0;

                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        index = y * width + x;

                        var surroundingPixels = GetSurroundingElements(x, y, AdaptiveThresholdSurroundSize);
                        var mean = (int)((surroundingPixels.Sum() / (float)surroundingPixels.Count));
                        thresholded[index] = Data[index] >= mean ? BackgroundPixel : ForegroundPixel;
                    }
                }

                return new Image(width, height, thresholded);
            }
            catch(System.Exception ex)
            {
                Debug.LogException(ex);
                return null;
            }
        }

        /// <summary>
        /// Process threshold on this image and return a new one
        /// </summary>
        /// <param name="threshold"></param>
        /// <returns></returns>
        public Image Threshold(int threshold)
        {
            int[] thresholded = new int[Data.Length];

            for (var i = 0; i < Data.Length; i++)
            {
                thresholded[i] = Data[i] >= threshold ?
                   BackgroundPixel : ForegroundPixel;
            }

            return new Image(width, height, thresholded);
        }
    }
}