﻿

public enum GrayscaleType
{
    Average,
    Lightness,
    Lumosity
}

public enum MorphologicalOperationType
{
    None,
    Dilate,
    Erode,
    Open,
    Close
}
