﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PiotrMitrega.Ocr
{
    public class Array2D
    {
        public int width;
        public int height;

        public int[] Data { get; private set; }

        public int this[int row, int column]
        {
            get { return Data[PositionToIndex(new Vector2(row, column))]; }
            set { Data[PositionToIndex(new Vector2(row, column))] = value; }
        }

        public Array2D(int width, int height, int[] data)
        {
            this.width = width;
            this.height = height;
            Data = data;
        }

        /// <summary>
        /// Gets Vector2 coordinates from array index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Vector2 IndexToPosition(int index)
        {
            return new Vector2(
                index % width,
                index / width);
        }

        /// <summary>
        /// Gets position in array from Vector2 coordinates
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public int PositionToIndex(Vector2 position)
        {
            int index = (int)(position.y * width + position.x);
            if (index >= Data.Length) return -1;
            return index;
        }
        
        /// <summary>
        /// Gets row of image
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public int[] GetRow(int row)
        {
            List<int> elements = new List<int>();
            for (int i = 0; i < width; i++)
            {
                elements.Add(GetElement(i, row));
            }

            return elements.ToArray();
        }
        
        /// <summary>
        /// Gets column of image
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public int[] GetColumn(int column)
        {
            List<int> elements = new List<int>();
            for (int i = 0; i < height; i++)
            {
                elements.Add(GetElement(column, i));
            }

            return elements.ToArray();
        }
        
        /// <summary>
        /// Gets element at specified x, y
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int GetElement(int x, int y)
        {
            int index = y * width + x;
            if (index > Data.Length - 1 || index < 0)
            {
                return 0;
            }
            else
            {
                return Data[index];
            }
        }

        /// <summary>
        /// Gets elements surrounding current one
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="size">Size of surrounding window</param>
        /// <param name="includeItself">Determines if given element should be included in return array</param>
        /// <returns></returns>
        public List<int> GetSurroundingElements(int x, int y, int size = 1, bool includeItself = false)
        {
            List<int> surroundingPixels = new List<int>();

            for (int yOffset = y - size; yOffset <= y + size; yOffset++)
            {
                if (yOffset < 0 || yOffset >= height) continue;

                for (int xOffset = x - size; xOffset <= x + size; xOffset++)
                {
                    if (xOffset < 0 || xOffset >= width) continue;

                    surroundingPixels.Add(Data[yOffset * width + xOffset]);
                }
            }

            if (includeItself)
            {
                surroundingPixels.Add(Data[y * width + x]);
            }

            return surroundingPixels;
        }        
    }
}