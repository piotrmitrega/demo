﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Collections.Generic;

namespace PiotrMitrega.Ocr
{
    public static class MorphologicalOperations
    {
        public static Image Dilate(Image input)
        {
            var processed = new int[input.Data.Length];
            var index = 0;

            for (var y = 0; y < input.height; y++)
            {
                for (var x = 0; x < input.width; x++)
                {
                    index = y * input.width + x;

                    if (input.Data[index] == Image.ForegroundPixel) continue;
                    processed[index] = Image.BackgroundPixel;

                    if (input.GetSurroundingElements(x, y).Any(p => p == 0))
                    {
                        processed[index] = Image.ForegroundPixel;
                    }
                }
            }

            return new Image(input.width, input.height, processed);
        }

        public static Image Erode(Image input)
        {
            var processed = new int[input.Data.Length];
            var index = 0;

            for (var y = 0; y < input.height; y++)
            {
                for (var x = 0; x < input.width; x++)
                {
                    index = y * input.width + x;
                    processed[index] = Image.BackgroundPixel;

                    if (input.Data[index] == 1) continue;
                    processed[index] = Image.ForegroundPixel;

                    if (input.GetSurroundingElements(x, y).Any(p => p == 1))
                    {
                        processed[index] = Image.BackgroundPixel;
                    }
                }
            }

            return new Image(input.width, input.height, processed);
        }

        public static Image Close(Image input)
        {
            return Dilate(Erode(input));
        }

        public static Image Open(Image input)
        {
            return Erode(Dilate(input));
        }
    }
}